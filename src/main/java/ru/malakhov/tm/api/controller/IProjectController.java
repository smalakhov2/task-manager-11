package ru.malakhov.tm.api.controller;

public interface IProjectController {

    void displayProjects();

    void clearProjects();

    void createProject();

}
