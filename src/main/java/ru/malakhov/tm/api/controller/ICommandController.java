package ru.malakhov.tm.api.controller;

public interface ICommandController {

    void printAbout();

    void printHelp();

    void printVersion();

    void printCommands();

    void printArguments();

    void printInfo();

    void exit();

}
