package ru.malakhov.tm.api.controller;

public interface ITaskController {

    void displayTasks();

    void clearTasks();

    void createTask();

}
