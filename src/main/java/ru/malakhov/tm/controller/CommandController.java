package ru.malakhov.tm.controller;

import ru.malakhov.tm.api.controller.ICommandController;
import ru.malakhov.tm.api.service.ICommandService;
import ru.malakhov.tm.model.Command;
import ru.malakhov.tm.util.NumberUtil;

import java.util.Arrays;

public class CommandController implements ICommandController {

    private ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    public void printAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name - Sergei malakhov");
        System.out.println("Email - smalakhov2@rencredit.ru");
    }

    public void printHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }

    public void printVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.1.1");
    }

    public void printCommands() {
        final String[] commands = commandService.getCommands();
        System.out.println(Arrays.toString(commands));
    }

    public void printArguments() {
        final String[] arguments = commandService.getArgs();
        System.out.println(Arrays.toString(arguments));
    }

    public void printInfo() {
        System.out.println("[INFO]");

        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM: " + usedMemoryFormat);
    }

    public void exit() {
        System.exit(0);
    }

}
