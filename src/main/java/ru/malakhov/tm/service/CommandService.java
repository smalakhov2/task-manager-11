package ru.malakhov.tm.service;

import ru.malakhov.tm.api.repository.ICommandRepository;
import ru.malakhov.tm.api.service.ICommandService;
import ru.malakhov.tm.model.Command;

public class CommandService implements ICommandService {

    private ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    public String[] getCommands() {
        return commandRepository.getCommands();
    }

    public String[] getArgs() {
        return commandRepository.getArgs();
    }

}